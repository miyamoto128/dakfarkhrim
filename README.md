Dakfarkhrim aims to be a fast Key/value store using a Restful interface.

## Usage

Dakfarkhrim stores values on various collections. 
Those collections behave as maps: they are maps!
Their data are saved at shutdown (using SIGTERM signal) in _*.db_ files.

Once running Dakfarkhrim listens on port 8443 and accepts the SSL requests below:

* GET https://localhost:8443/q/{collection}/{id}             -> read value
* PUT https://localhost:8443/q/{collection}/{id}/{value}     -> add/modify value
* DELETE https://localhost:8443/q/{collection}/{id}          -> remove value

### Examples
Using curl (https://curl.haxx.se/)

`curl -ik -X PUT https://localhost:8443/q/my_collection/id_1/my_value`

Add _my_value_ into collection _my_collection_ identified by _id_1_.

`curl -ik https://localhost:8443/q/my_collection/id_1`

Retrieve and return the value _my_value_.

`curl -ik -X DELETE https://localhost:8443/q/my_collection/id_1`

Remove the retrieved value _my_value_.

## Build

Install rust and cargo: https://www.rust-lang.org/tools/install.

Install openssl: https://docs.rs/openssl/0.10.28/openssl/#automatic 

Build the executable :

`cargo build`

## Run tests

`$ cargo test`

## Execute 
Before first execution, create a self-signed temporary certificate:

`openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'`

Then create the data directory with read/write permissions:

`/var/lib/dakfarkhrim`

Finally, to execute:

`cargo run`


## Execute using docker

The files key.pem and cert.pem must be present in the Dockerfile directory (using openssl as described above).

### docker-compose
Build the image:
`docker-compose build`

Execute container:
`docker-compose up`

Ensure data persistence in internal volume.

### docker
Build the image:
`docker build -t dakfarkhrim .`

Execute container:
`docker run -p 8080:8080 -p 8443:8443 dakfarkhrim`
