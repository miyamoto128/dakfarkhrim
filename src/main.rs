#[macro_use]
extern crate log;

mod storage;
use storage::front::KvStore;

use actix_web::{App, web, HttpResponse, HttpServer, middleware::Logger};
use env_logger::Env;
use openssl::ssl::{SslAcceptor, SslAcceptorBuilder, SslFiletype, SslMethod};

use std::path::PathBuf;
use std::sync::Mutex;


// TODO use a configuration file for those parameters
const LISTEN_HOST: &str = "0.0.0.0";
const LISTEN_PORT: u16 = 8080;
const LISTEN_PORT_SSL: u16 = 8443;
const DB_DIR: &str = "/var/lib/dakfarkhrim";
const SSL_KEY_FILE: &str = "key.pem";
const SSL_CERT_FILE: &str = "cert.pem";


pub struct AppData {
    kv_store: Mutex<KvStore>,
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::from_env(Env::default().default_filter_or("info")).init();

    info!("Starting up");

    let kv_store = KvStore::new(PathBuf::from(DB_DIR));
    let app_data = web::Data::new(AppData {
        kv_store: Mutex::new(kv_store),
    });

    let listen_address = format!("{}:{}", LISTEN_HOST, LISTEN_PORT);
    let listen_address_ssl = format!("{}:{}", LISTEN_HOST, LISTEN_PORT_SSL);

    // load ssl keys
    // to create a self-signed temporary cert for testing:
    // `openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'`
    let builder = load_ssl_keys(SSL_KEY_FILE, SSL_CERT_FILE);

    let result = HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .app_data(app_data.clone())
            // queries
            .service(
                web::scope("/q")
                    .route("/{collection}/{id}", web::get().to(crud::get))
                    .route("/{collection}/{id}/{data}", web::put().to(crud::set))
                    .route("/{collection}/{id}", web::put().to(crud::set_from_body))
                    .route("/{collection}/{id}", web::delete().to(crud::remove))
            )
            // describe
            .service(
                web::scope("/describe")
                    .route("", web::get().to(describe::list_collections))
                    .route("/{collection}", web::get().to(describe::collection))
            )
            // info
            .service(
                web::scope("/info")
                    .route("/is_alive", web::get().to(|| HttpResponse::Ok().body("UP")))
            )
            // administration
            .service(
                web::scope("/tool")
                    .route("/bench", web::get().to(tool::bench))
                    .route("/flush", web::get().to(tool::save_kvstore))
            )
    })
    .bind(listen_address)?
    .bind_openssl(listen_address_ssl, builder)?
    .run()
    .await;

    info!("Shutdown");

    result
}

fn load_ssl_keys(key_file: &str, cert_file: &str) -> SslAcceptorBuilder {
    let mut builder =
        SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder
        .set_private_key_file(key_file, SslFiletype::PEM)
        .unwrap();
    builder.set_certificate_chain_file(cert_file).unwrap();
    builder
}


pub mod crud {
    use crate::storage::front::{KvElement, mime};
    use super::AppData;

    use actix_web::{web, HttpRequest, HttpMessage, HttpResponse, Error, Result};
    use futures::StreamExt;


    // Dakfarkhrim custom header
    const CUSTOM_HEADER_CONTENT_TTL: &str = "Content-Time-To-Live"; // Content TTL in seconds


    pub async fn get(data: web::Data<AppData>, info: web::Path<(String, String)>) -> HttpResponse {
        let kv_store = data.kv_store.lock().unwrap();

        match kv_store.lookup(&info.0, &info.1) {
            Some(element) => {
                if element.is_expired() {
                    HttpResponse::NotFound().finish()
                } else {
                    HttpResponse::Ok()
                        .content_type(&element.mime)
                        .set_header(&String::from(CUSTOM_HEADER_CONTENT_TTL), element.ttl_in_seconds())
                        .body(element.data.clone())
                }
            },
            None => HttpResponse::NotFound().finish(),
        }
    }

    pub async fn set(
        data: web::Data<AppData>,
        req: HttpRequest,
        info: web::Path<(String, String, String)>
    ) -> HttpResponse {

        let (_, content_ttl) = extract_headers(&req);

        let mut element = KvElement::from_string(info.1.clone(), info.2.clone());
        element.set_ttl(content_ttl * 1000);

        do_set(&data, &info.0, &element)
    }

    pub async fn set_from_body(
        data: web::Data<AppData>,
        req: HttpRequest,
        info: web::Path<(String, String)>,
        mut body: web::Payload
    ) -> Result<HttpResponse, Error> {

        let (content_type, content_ttl) = extract_headers(&req);

        let mut bytes = web::BytesMut::new();
        while let Some(item) = body.next().await {
            bytes.extend_from_slice(&item?);
        }

        let mut element = KvElement::new(info.1.clone(), bytes.to_vec(), content_type);
        element.set_ttl(content_ttl * 1000);

        Ok(do_set(&data, &info.0, &element))
    }

    pub async fn remove(data: web::Data<AppData>, info: web::Path<(String, String)>) -> HttpResponse {
        let mut kv_store = data.kv_store.lock().unwrap();

        match kv_store.remove(&info.0, &info.1) {
            Some(_) => HttpResponse::NoContent().finish(),
            None => HttpResponse::NotFound().finish(),
        }
    }


    fn extract_headers(req: &HttpRequest) -> (String, u64) {
        let default = mime::OCTET_STREAM.to_string();
        let content_type = match req.mime_type() {
            Ok(result) => match result {
                Some(mime) => mime.to_string(),
                None => default,
            },
            _ => default,
        };

        let default = 0;
        let content_ttl :u64 = match req.headers().get(CUSTOM_HEADER_CONTENT_TTL) {
            Some(value) => match value.to_str() {
                Ok(s) => s.to_string().parse().unwrap_or(default),
                _ => default,
            },
            _ => default,
        };

        (content_type, content_ttl)
    }

    fn do_set(data: &web::Data<AppData>, collection_name: &String, element: &KvElement) -> HttpResponse {
        let mut kv_store = data.kv_store.lock().unwrap();

        let is_new_element = (*kv_store).upsert(collection_name, element.to_owned());
        if is_new_element {
            return HttpResponse::Created()
                .set_header(&String::from(CUSTOM_HEADER_CONTENT_TTL), element.ttl_in_seconds())
                .finish();
        }
        HttpResponse::NoContent()
            .set_header(&String::from(CUSTOM_HEADER_CONTENT_TTL), element.ttl_in_seconds())
            .finish()
    }
}

pub mod describe {
    use super::AppData;

    use actix_web::{web, HttpResponse};
    use serde::{Deserialize, Serialize};


    #[derive(Serialize, Deserialize)]
    struct ListCollections {
        collections: Vec<String>,
        count: usize,
    }

    pub async fn list_collections(data: web::Data<AppData>) -> HttpResponse  {
        let kv_store = data.kv_store.lock().unwrap();

        let mut collections = Vec::new();
        for name in kv_store.collections_names() {
            collections.push(name.clone());
        }

        HttpResponse::Ok().json(ListCollections {
            count: collections.len(),
            collections,
        })
    }

    #[derive(Serialize, Deserialize)]
    struct CollectionDetails {
        first_id: String,
        last_id: String,
        count: usize,
    }

    pub async fn collection(data: web::Data<AppData>, info: web::Path<String>) -> HttpResponse {
        let kv_store = data.kv_store.lock().unwrap();

        match kv_store.collection_details(&info) {
            Some((first_id, last_id, length)) => HttpResponse::Ok().json(CollectionDetails {
                first_id,
                last_id,
                count: length,
            }),
            None => HttpResponse::NotFound().finish()
        }
    }
}

pub mod tool {
    use crate::storage;
    use super::AppData;

    use actix_web::{web, HttpResponse, Result, Responder};


    // TODO used for testing, remove later on
    pub async fn bench() -> Result<String> {
        let bench_report = storage::benchmarks::benchmark_collection();
        Ok(bench_report)
    }

    // TODO used for testing, remove later on
    pub async fn save_kvstore(data: web::Data<AppData>) -> impl Responder {
        let kv_store = data.kv_store.lock().unwrap();
        kv_store.save();
        HttpResponse::Ok().body("OK")
    }
}
