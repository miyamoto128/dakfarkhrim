use log::{info, warn};

use std::fs;
use std::path::{Path, PathBuf};

pub fn prefix_matching_files(db_dir: &Path, file_name_pattern: &str, file_name_prefix: &str) {
    let entries = match fs::read_dir(db_dir) {
        Ok(entries) => entries,
        Err(why) => {
            warn!("Could not open directory {} - {}", db_dir.display(), why);
            return;
        }
    };

    let re = regex::Regex::new(file_name_pattern).unwrap();

    let files = entries
        .map(|res| res.unwrap().path())
        .filter(|path| re.is_match(path.file_name().unwrap().to_str().unwrap()))
        .collect::<Vec<PathBuf>>();

    for file in files {
        let mut new_file_name = String::from(file_name_prefix);
        new_file_name.push_str(file.file_name().unwrap().to_str().unwrap());

        let mut new_file = file.clone();
        new_file.pop();
        new_file.push(new_file_name);

        match fs::rename(&file, &new_file) {
            Ok(()) => info!("Rename {} to {}", file.display(), new_file.display()),
            Err(why) => warn!("Could not rename {} - {} ", file.display(), why),
        }
    }
}

pub fn remove_matching_files(db_dir: &Path, file_name_pattern: &str) {
    let entries = match fs::read_dir(db_dir) {
        Ok(entries) => entries,
        Err(why) => {
            warn!("Could not open directory {} - {}", db_dir.display(), why);
            return;
        }
    };

    let re = regex::Regex::new(file_name_pattern).unwrap();

    let files = entries
        .map(|res| res.unwrap().path())
        .filter(|path| re.is_match(path.file_name().unwrap().to_str().unwrap()))
        .collect::<Vec<PathBuf>>();

    for file in files {
        match fs::remove_file(&file) {
            Ok(()) => info!("Remove {}", file.display()),
            Err(why) => warn!("Could not remove {} - {} ", file.display(), why),
        }
    }
}

#[cfg(test)]
mod tests_file_utils {
    use std::fs;
    use std::path::{Path, PathBuf};

    #[test]
    fn prefix_matching_files() -> std::io::Result<()> {
        let db_dir = Path::new("tests/tmp/prefix_matching_files");

        create_empty_file(db_dir, "empty_1.txt")?;
        create_empty_file(db_dir, "empty_2.txt")?;
        create_empty_file(db_dir, "empty_123456.txt.bak")?;
        create_empty_file(db_dir, "abcd.txt")?;

        super::prefix_matching_files(db_dir, r"^empty_\d+\.txt$", "PREFIX_");

        assert!(!Path::new("tests/tmp/prefix_matching_files/empty_1.txt").exists());
        assert!(!Path::new("tests/tmp/prefix_matching_files/empty_2.txt").exists());
        assert!(Path::new("tests/tmp/prefix_matching_files/empty_123456.txt.bak").exists());
        assert!(Path::new("tests/tmp/prefix_matching_files/abcd.txt").exists());

        assert!(Path::new("tests/tmp/prefix_matching_files/PREFIX_empty_1.txt").exists());
        assert!(Path::new("tests/tmp/prefix_matching_files/PREFIX_empty_2.txt").exists());

        fs::remove_dir_all(db_dir)?; // clean test dir
        Ok(())
    }

    #[test]
    fn remove_matching_files() -> std::io::Result<()> {
        let db_dir = Path::new("tests/tmp/remove_matching_files");

        create_empty_file(db_dir, "empty_1.txt")?;
        create_empty_file(db_dir, "empty_2.txt")?;
        create_empty_file(db_dir, "empty_123456.txt.bak")?;
        create_empty_file(db_dir, "abcd.txt")?;

        super::remove_matching_files(db_dir, r"^empty_\d+\.txt$");

        assert!(!Path::new("tests/tmp/remove_matching_files/empty_1.txt").exists());
        assert!(!Path::new("tests/tmp/remove_matching_files/empty_2.txt").exists());
        assert!(Path::new("tests/tmp/remove_matching_files/empty_123456.txt.bak").exists());
        assert!(Path::new("tests/tmp/remove_matching_files/abcd.txt").exists());

        fs::remove_dir_all(db_dir)?; // clean test dir
        Ok(())
    }

    fn create_empty_file(dir: &Path, file_name: &str) -> std::io::Result<()> {
        let mut path: PathBuf = dir.to_path_buf();
        path.push(file_name);

        fs::create_dir_all(dir)?;
        fs::write(path, "")?;
        Ok(())
    }
}
