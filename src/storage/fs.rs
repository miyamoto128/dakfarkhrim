use super::{file_utils, front::KvElement, mem::KvCollection, utils};

use log::{info, warn};
use serde::{Deserialize, Serialize};

use std::collections::BTreeMap;
use std::fs::{read_dir, DirBuilder, File};
use std::io::prelude::*;
use std::path::{Path, PathBuf};

const DB_FORMAT_ID: &str = "DKF.KvCollection";
const DB_FORMAT_VERSION: &str = "0.1";

const DB_FILENAME_EXT: &str = "db";
const DB_FILENAME_PREFIX_DEPRECATED: &str = "__DEPRECATED__";


pub fn load_collections(collections: &mut BTreeMap<String, KvCollection>, db_dir: &Path) {
    collections.clear();

    let entries = match read_dir(db_dir) {
        Ok(entries) => entries,
        Err(why) => {
            warn!("Could not open directory {} - {}", db_dir.display(), why);
            return;
        }
    };

    let mut db_files = entries
        .map(|res| res.unwrap().path())
        .filter(|path| path.extension().unwrap_or(std::ffi::OsStr::new("")) == DB_FILENAME_EXT)
        .collect::<Vec<PathBuf>>();

    db_files.sort();

    for path in db_files {
        load_from_block(collections, &path);
    }
}

pub fn save_collections(collections: &BTreeMap<String, KvCollection>, db_dir: &Path) {
    DirBuilder::new().recursive(true).create(db_dir).unwrap();

    for entry in collections {
        let collection_name = entry.0;
        let collection = entry.1;

        let prefix_pattern = format!(r"^{}.*\.{}", collection_name, DB_FILENAME_EXT);
        let remove_pattern = format!(
            r"^{}\w+.*\.{}",
            DB_FILENAME_PREFIX_DEPRECATED, DB_FILENAME_EXT
        );

        // Remove in 2 steps: 1) rename files to remove; save collection files; 2) remove renamed files
        //
        // Thus, in case the save collection gets interrupted we can differentiate older and newer
        // collection files.

        file_utils::prefix_matching_files(&db_dir, &prefix_pattern, DB_FILENAME_PREFIX_DEPRECATED);
        save_collection(&collection, collection_name, &db_dir);
        file_utils::remove_matching_files(db_dir, &remove_pattern);
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct BareKvCollection {
    format: String,
    version: String,
    name: String,
    elements: Vec<KvElement>,
}

fn load_from_block(collections: &mut BTreeMap<String, KvCollection>, path: &Path) {
    let display = path.display();

    info!("Loading {} ...", display);

    let mut serialized_collection: Vec<u8> = Vec::new();
    let mut file = File::open(path).expect(format!("Couldn't open {}", display).as_str());
    file.read_to_end(&mut serialized_collection)
        .expect(format!("Couldn't read {}", display).as_str());

    let bare_collection: BareKvCollection =
        bincode::deserialize(&serialized_collection[..]).unwrap();
    let collection_name = bare_collection.name;

    if !collections.contains_key(&collection_name) {
        collections.insert(collection_name.clone(), KvCollection::new());
    }
    let current_collection = collections.get_mut(&collection_name).unwrap();

    for element in bare_collection.elements {
        current_collection.insert(element);
    }

    // TODO improve by using a filter on element.is_expired at deserialization instead
    // This code aims to use and experiment the clear_expired method.
    let nb_loaded = current_collection.len();
    current_collection.clear_expired();
    let nb_filtered = current_collection.len();

    info!(
        "Loaded {} {}/{} item(s) from {}",
        collection_name, nb_filtered, nb_loaded, display
    );
}

fn save_collection(collection: &KvCollection, name: &String, db_dir: &Path) {
    info!("Saving {}...", name);

    // Split and save as sub collections
    let mut count_saved: usize = 0;

    let mut current_elements = Vec::new();
    let mut current_data_size: usize = 0;
    let block_data_size = utils::memory_available() / 5 + 1;

    info!("> using block data size: {} b ...", block_data_size);

    for (_, element) in collection.iter() {
        // No need to save expired elements
        if element.is_expired() {
            continue;
        }

        current_elements.push(element.clone());
        current_data_size += element.data.len();

        if (current_data_size as u64) >= block_data_size {
            count_saved += save_blocks(current_elements, name, db_dir);
            current_elements = Vec::new();
            current_data_size = 0;
        }
    }

    if !current_elements.is_empty() {
        count_saved += save_blocks(current_elements, name, db_dir);
    }

    let count_to_save = collection.len();
    info!("Saved {} {}/{} item(s)", name, count_saved, count_to_save);
}

fn save_blocks(elements: Vec<KvElement>, collection_name: &String, db_dir: &Path) -> usize {
    let count_elements = elements.len();

    let sub_collection = BareKvCollection {
        format: String::from(DB_FORMAT_ID),
        version: String::from(DB_FORMAT_VERSION),
        name: collection_name.clone(),
        elements,
    };
    let serialized_collection: Vec<u8> = bincode::serialize(&sub_collection).unwrap();

    let db_path = compose_db_path(db_dir, collection_name);
    let display = db_path.display();

    let mut file = File::create(&db_path).expect(format!("Couldn't create {}", display).as_str());

    match file.write(serialized_collection.as_slice()) {
        Err(why) => warn!("Couldn't save collection {}: {}", display, why),
        Ok(_) => (),
    };

    info!(
        "> saved {} {} item(s) into {}",
        collection_name, count_elements, display
    );

    count_elements
}

fn compose_db_path(db_dir: &Path, collection_name: &str) -> PathBuf {
    let mut filename = String::from(collection_name);
    filename.push_str("-");
    filename.push_str(&utils::now_as_millis().to_string());

    let mut db_path = PathBuf::from(db_dir);
    db_path.push(filename);
    db_path.set_extension(DB_FILENAME_EXT);
    db_path
}

#[cfg(test)]
mod tests_file_system {
    use crate::storage::mem;
    use crate::storage::mem::KvCollection;
    use crate::storage::utils;

    use std::collections::BTreeMap;
    use std::path::Path;

    #[test]
    fn save_and_load_all_collections() -> std::io::Result<()> {
        const DB_TEST_PATH: &str = "tests/tmp/save_and_load_all_collections";
        let db_dir = Path::new(DB_TEST_PATH);

        let mut collections = create_collections();

        println!(
            "Saving collection \"first\" -> {:?}",
            collections.get("first").unwrap()
        );
        super::save_collections(&collections, db_dir);

        assert_eq!(2, count_matching_files(db_dir, ".+"));
        assert_eq!(1, count_matching_files(db_dir, r"^first-\d+\.db$"));
        assert_eq!(1, count_matching_files(db_dir, r"^second-\d+\.db$"));
        assert_eq!(0, count_matching_files(db_dir, r"^third-\d+\.db$"));

        collections.clear();
        assert!(collections.is_empty());

        super::load_collections(&mut collections, db_dir);
        println!(
            "Loaded collection \"first\" -> {:?}",
            collections.get("first").unwrap()
        );
        assert_eq!(2, collections.len());

        assert_eq!(10, collections.get("first").unwrap().len());
        assert_eq!(5, collections.get("second").unwrap().len());
        assert!(!collections.contains_key("third"));

        let first = collections.get("first").unwrap();
        assert_eq!(
            "value_1".as_bytes(),
            first.get("id_1").unwrap().data.as_slice()
        );
        assert_eq!(
            "value_5".as_bytes(),
            first.get("id_5").unwrap().data.as_slice()
        );

        std::fs::remove_dir_all(db_dir)?; // clean test dir
        Ok(())
    }

    #[test]
    fn filter_expired_data_at_save() -> std::io::Result<()> {
        const DB_TEST_PATH: &str = "tests/tmp/filter_expired_data_at_save";
        const COLLECTION_NAME: &str = "expiring_elements";
        let db_dir = Path::new(DB_TEST_PATH);

        // Create and fill collection
        let mut collections = BTreeMap::new();
        let mut collection = KvCollection::new();

        let mut elt1 = super::KvElement::from_string(
            String::from("very_short_ttl"),
            String::from("some_data"),
        );
        let mut elt2 =
            super::KvElement::from_string(String::from("long_ttl"), String::from("some_data"));
        elt1.set_ttl(1); // short TTL
        elt2.set_ttl(10_000); // long TTL

        collection.insert(elt1);
        collection.insert(elt2);
        assert_eq!(2, collection.len());

        collections.insert(String::from(COLLECTION_NAME), collection);
        assert_eq!(1, collections.len());

        // Wait for short TTL to expire
        utils::sleep(5);

        // Save into db files excluding expired element (with short TTL)
        super::save_collections(&collections, db_dir);

        // Load the just saved db files
        super::load_collections(&mut collections, db_dir);
        let collection = collections.get(COLLECTION_NAME).unwrap();
        assert_eq!(1, collection.len());
        assert_eq!(None, collection.get("short_ttl"));
        assert_ne!(None, collection.get("long_ttl"));

        std::fs::remove_dir_all(db_dir)?; // clean test dir
        Ok(())
    }

    #[test]
    fn filter_expired_data_at_load() -> std::io::Result<()> {
        const DB_TEST_PATH: &str = "tests/tmp/filter_expired_data_at_load";
        const COLLECTION_NAME: &str = "expiring_elements";
        let db_dir = Path::new(DB_TEST_PATH);

        // Create and fill collection
        let mut collections = BTreeMap::new();
        let mut collection = KvCollection::new();

        let mut elt1 = super::KvElement::from_string(
            String::from("very_short_ttl"),
            String::from("some_data"),
        );
        let mut elt2 =
            super::KvElement::from_string(String::from("long_ttl"), String::from("some_data"));
        elt1.set_ttl(100); // short TTL
        elt2.set_ttl(10_000); // long TTL

        collection.insert(elt1);
        collection.insert(elt2);
        assert_eq!(2, collection.len());

        collections.insert(String::from(COLLECTION_NAME), collection);
        assert_eq!(1, collections.len());

        // Save and load into db files
        super::save_collections(&collections, db_dir);
        super::load_collections(&mut collections, db_dir);
        let collection = collections.get(COLLECTION_NAME).unwrap();
        assert_eq!(2, collection.len());

        // Wait for short TTL to expire
        utils::sleep(200);

        // Reload excluding expired element (with short TTL)
        super::load_collections(&mut collections, db_dir);
        let collection = collections.get(COLLECTION_NAME).unwrap();
        assert_eq!(1, collection.len());
        assert_eq!(None, collection.get("short_ttl"));
        assert_ne!(None, collection.get("long_ttl"));

        std::fs::remove_dir_all(db_dir)?; // clean test dir
        Ok(())
    }

    #[test]
    fn compose_db_path() {
        let db_path = super::compose_db_path(Path::new("dir1/dir2/dir3"), "collection_name");

        let re = regex::Regex::new(r"^dir1/dir2/dir3/collection_name-\d+\.db$").unwrap();
        assert!(re.is_match(db_path.to_str().unwrap()));
    }

    pub fn count_matching_files(src_dir: &Path, file_name_pattern: &str) -> usize {
        let entries = match std::fs::read_dir(src_dir) {
            Ok(entries) => entries,
            Err(why) => {
                warn!("Could not open directory {} - {}", src_dir.display(), why);
                return 0;
            }
        };

        let re = regex::Regex::new(file_name_pattern).unwrap();

        let files = entries
            .map(|res| res.unwrap().path())
            .filter(|path| re.is_match(path.file_name().unwrap().to_str().unwrap()))
            .collect::<Vec<std::path::PathBuf>>();

        files.len()
    }

    // Create map containing collections "first", "second" and "third"
    fn create_collections() -> BTreeMap<String, KvCollection> {
        let mut collections = BTreeMap::new();
        let first_collection = mem::create_test_collection(10);
        let second_collection = mem::create_test_collection(5);
        let third_collection = mem::create_test_collection(0);

        collections.insert(String::from("first"), first_collection);
        collections.insert(String::from("second"), second_collection);
        collections.insert(String::from("third"), third_collection);

        collections
    }
}
