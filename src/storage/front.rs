use super::{fs, mem::KvCollection, utils};

use serde::{Deserialize, Serialize};

use std::collections::BTreeMap;
use std::path::PathBuf;

pub mod mime {
    pub const PLAIN_TEXT: &str = "text/plain";
    pub const OCTET_STREAM: &str = "application/octet-stream";
}

pub struct KvStore {
    db_path: PathBuf,
    collections: BTreeMap<String, KvCollection>,
}

impl KvStore {
    pub fn new(db_path: PathBuf) -> Self {
        let mut collections = BTreeMap::new();
        fs::load_collections(&mut collections, &db_path);
        Self {
            db_path,
            collections,
        }
    }

    pub fn lookup(&self, collection_name: &str, id: &str) -> Option<&KvElement> {
        match self.collections.get(collection_name) {
            Some(collection) => collection.get(id),
            None => None,
        }
    }

    // Return true if created new element
    pub fn upsert(&mut self, collection_name: &str, element: KvElement) -> bool {
        match self.collections.get_mut(collection_name) {
            Some(collection) => collection.insert(element),
            None => {
                let mut collection = KvCollection::new();
                let is_new_element = collection.insert(element);
                self.collections
                    .insert(String::from(collection_name), collection);
                is_new_element
            }
        }
    }

    pub fn remove(&mut self, collection_name: &str, id: &str) -> Option<KvElement> {
        match self.collections.get_mut(collection_name) {
            Some(collection) => {
                let element = collection.remove(id);
                if collection.len() == 0 {
                    self.collections.remove(collection_name);
                }
                element
            },
            None => None,
        }
    }

    pub fn collections_names(&self) -> Vec<&String> {
        self.collections.keys().collect()
    }

    pub fn collection_details(&self, collection_name: &str) -> Option<(String, String, usize)> {
        match self.collections.get(collection_name) {
            Some(collection) => {
                if collection.len() > 0 {
                    return Some((
                        collection.first().unwrap().id.clone(),
                        collection.last().unwrap().id.clone(),
                        collection.len(),
                    ));
                }
                None
            }
            None => None,
        }
    }

    pub fn save(&self) {
        fs::save_collections(&self.collections, &self.db_path);
    }
}

impl Drop for KvStore {
    fn drop(&mut self) {
        self.save();
    }
}

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub struct KvElement {
    pub id: String,
    pub data: Vec<u8>,
    pub mime: String,
    creation: u64,
    pub last_modified: u64,
    pub expiration: u64, // 0 means no expiration
}

impl KvElement {
    pub fn new(id: String, data: Vec<u8>, mime: String) -> KvElement {
        KvElement {
            id,
            data,
            mime,
            creation: utils::now_as_millis(),
            last_modified: 0,
            expiration: 0,
        }
    }

    pub fn from_string(id: String, data: String) -> KvElement {
        Self::new(id, data.into_bytes(), String::from(mime::PLAIN_TEXT))
    }

    pub fn set_ttl(&mut self, ttl: u64) {
        if ttl > 0 {
            self.expiration = self.creation + ttl;
        }
    }

    pub fn ttl_in_seconds(&self) -> u64 {
        if self.expiration == 0 {
            return std::u64::MAX;
        }

        let now = utils::now_as_millis();
        if now >= self.expiration {
            return 0;
        }

        (self.expiration - now) / 1000
    }

    pub fn is_expired(&self) -> bool {
        self.expiration != 0 && self.expiration < utils::now_as_millis()
    }
}

#[cfg(test)]
mod tests_kv_store {
    use super::KvElement;
    use super::KvStore;

    use std::path::Path;

    #[test]
    fn lookups_ok() {
        let db_path = Path::new("tests/tmp/lookups_ok");
        let mut store = create_empty_kvstore(db_path);

        let new_element = KvElement::from_string(String::from("id"), String::from("abcdef"));
        store.upsert("collection1", new_element);
        let new_element =
            KvElement::from_string(String::from("other_id"), String::from("whatever"));
        store.upsert("collection2", new_element);

        assert_ne!(None, store.lookup("collection1", "id"));

        let found_element = store.lookup("collection2", "other_id").unwrap();
        assert_eq!("other_id", &found_element.id);
        assert_eq!("whatever".as_bytes(), found_element.data.as_slice());
        assert_eq!("text/plain", &found_element.mime);
    }

    #[test]
    fn lookups_none() {
        let db_path = Path::new("tests/tmp/lookups_none");
        let mut store = create_empty_kvstore(db_path);

        let new_element = KvElement::from_string(String::from("id"), String::from("abcdef"));
        store.upsert("collection1", new_element);

        assert_eq!(None, store.lookup("collection1", "unknown_id"));
        assert_eq!(None, store.lookup("unknown_collection", "id"));
    }

    #[test]
    fn remove() {
        let db_path = Path::new("tests/tmp/remove");
        let mut store = create_empty_kvstore(db_path);

        let new_element = KvElement::from_string(String::from("id"), String::from("something"));
        store.upsert("collection1", new_element);
        assert_ne!(None, store.lookup("collection1", "id"));

        let removed = store.remove("collection1", "id").unwrap();
        assert_eq!("id", &removed.id);
        assert_eq!("something".as_bytes(), removed.data.as_slice());

        assert_eq!(None, store.lookup("collection1", "id"));
    }

    fn create_empty_kvstore(db_path: &Path) -> KvStore {
        KvStore::new(db_path.to_path_buf())
    }
}

#[cfg(test)]
mod tests_kv_element {
    use super::utils;
    use super::KvElement;

    static TINY_GIF: &[u8] = &[
        0x47, 0x49, 0x46, 0x38, 0x39, 0x61, 0x01, 0x00, 0x01, 0x00, 0x80, 0x00, 0x00, 0xff, 0xff,
        0xff, 0x00, 0x00, 0x00, 0x21, 0xf9, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x2c, 0x00, 0x00,
        0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x02, 0x02, 0x44, 0x01, 0x00, 0x3b,
    ];

    #[test]
    fn new() {
        let element = KvElement::new(
            String::from("my_tiny_gif"),
            TINY_GIF.to_vec(),
            String::from("image/gif"),
        );
        let now = utils::now_as_millis();

        assert_eq!("my_tiny_gif", &element.id);
        assert_eq!(TINY_GIF, element.data.as_slice());
        assert_eq!("image/gif", &element.mime);
        assert_eq!(now, element.creation);
        assert_eq!(0, element.last_modified);
        assert_eq!(0, element.expiration);
    }

    #[test]
    fn from_string() {
        let element = KvElement::from_string(String::from("my_text"), String::from("Bella ciao"));
        assert_eq!("my_text", &element.id);
        assert_eq!(b"Bella ciao", element.data.as_slice());
        assert_eq!("text/plain", &element.mime);
    }

    #[test]
    fn ttl() {
        let mut element = KvElement::new(
            String::from("my_tiny_gif"),
            TINY_GIF.to_vec(),
            String::from("image/gif"),
        );
        let now = utils::now_as_millis();

        assert_eq!(now, element.creation);
        assert_eq!(0, element.expiration);

        element.set_ttl(0);
        assert_eq!(0, element.expiration);

        element.set_ttl(3600_000);
        assert_eq!(now + 3600_000, element.expiration);
        assert_eq!(3600, element.ttl_in_seconds());
        assert!(!element.is_expired());
    }

    #[test]
    fn ttl_expired() {
        let mut element = KvElement::new(
            String::from("my_tiny_gif"),
            TINY_GIF.to_vec(),
            String::from("image/gif"),
        );
        let now = utils::now_as_millis();
        assert_eq!(now, element.creation);

        element.set_ttl(1000);
        assert_eq!(now + 1000, element.expiration);
        assert_eq!(1, element.ttl_in_seconds());
        assert!(!element.is_expired());

        utils::sleep(1001); // Wait for element to expire
        assert_eq!(0, element.ttl_in_seconds());
        assert!(element.is_expired());
    }
}
