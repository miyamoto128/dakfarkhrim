use super::{front::KvElement, utils};

use std::collections::btree_map::Iter;
use std::collections::{BTreeMap, BTreeSet};

#[derive(Debug)]
pub struct KvCollection {
    map: BTreeMap<String, KvElement>,
    ids_by_expiration: TtlContainer,
}

impl KvCollection {
    pub fn new() -> Self {
        Self {
            map: BTreeMap::new(),
            ids_by_expiration: TtlContainer::new(),
        }
    }

    pub fn get(&self, id: &str) -> Option<&KvElement> {
        match self.map.get(id) {
            Some(element) => Some(&element),
            None => None,
        }
    }

    pub fn insert(&mut self, element: KvElement) -> bool {
        match self.map.get_mut(&element.id) {
            Some(current) => {
                self.ids_by_expiration
                    .remove(current.expiration, &current.id);
                self.ids_by_expiration
                    .insert(element.expiration, element.id.clone());

                current.data = element.data;
                current.mime = element.mime;
                current.last_modified = utils::now_as_millis();
                current.expiration = element.expiration;
                false
            }
            None => {
                if !element.is_expired() {
                    // Save insert if element already expired
                    self.ids_by_expiration
                        .insert(element.expiration, element.id.clone());
                    self.map.insert(element.id.clone(), element);
                }
                true
            }
        }
    }

    pub fn remove(&mut self, id: &str) -> Option<KvElement> {
        match self.map.remove(id) {
            Some(element) => Some(element),
            None => None,
        }
    }

    pub fn clear(&mut self) {
        self.map.clear();
    }

    pub fn clear_expired(&mut self) {
        let now = utils::now_as_millis();
        let elapsed = self.ids_by_expiration.split_off_elapsed(&now);

        for (_timestamp, ids) in elapsed {
            for id in ids {
                self.remove(&id);
            }
        }
    }

    pub fn len(&self) -> usize {
        self.map.len()
    }

    pub fn first(&self) -> Option<&KvElement> {
        match self.map.iter().next() {
            Some((_k, v)) => Some(v),
            None => None,
        }
    }

    pub fn last(&self) -> Option<&KvElement> {
        match self.map.iter().next_back() {
            Some((_k, v)) => Some(v),
            None => None,
        }
    }

    pub fn iter(&self) -> Iter<String, KvElement> {
        self.map.iter()
    }
}

#[derive(Debug)]
struct TtlContainer {
    ids_by_timestamp: BTreeMap<u64, BTreeSet<String>>,
}

impl TtlContainer {
    fn new() -> Self {
        Self {
            ids_by_timestamp: BTreeMap::new(),
        }
    }

    fn insert(&mut self, timestamp_millis: u64, id: String) {
        // timestamp == 0 means no timestamp => no insert
        if timestamp_millis == 0 {
            return;
        }

        match self.ids_by_timestamp.get_mut(&timestamp_millis) {
            Some(set) => {
                set.insert(id);
            }
            None => {
                let mut set = BTreeSet::new();
                set.insert(id);
                self.ids_by_timestamp.insert(timestamp_millis, set);
            }
        };
    }

    fn remove(&mut self, timestamp_millis: u64, id: &String) {
        match self.ids_by_timestamp.get_mut(&timestamp_millis) {
            Some(set) => {
                set.remove(id);
            }
            None => {}
        };
    }

    fn split_off_elapsed(&mut self, elapsed_millis: &u64) -> BTreeMap<u64, BTreeSet<String>> {
        let mut elapsed = self.ids_by_timestamp.split_off(elapsed_millis);
        std::mem::swap(&mut elapsed, &mut self.ids_by_timestamp);
        elapsed
    }
}

// Create collection with predefined keys and values.
pub fn create_test_collection(items_count: usize) -> KvCollection {
    let mut collection = KvCollection::new();
    for i in 0..items_count {
        let id = format!("id_{}", i);
        let value = format!("value_{}", i);
        let element = KvElement::from_string(id, value);

        collection.insert(element);
    }
    collection
}

#[cfg(test)]
mod tests_collection {
    use super::KvCollection;
    use crate::storage::utils;

    #[test]
    fn lookups_ok() {
        let collection = super::create_test_collection(100);
        assert_eq!(100, collection.len());

        assert_eq!(
            "value_0".as_bytes(),
            collection.get("id_0").unwrap().data.as_slice()
        );
        assert_eq!(
            "value_50".as_bytes(),
            collection.get("id_50").unwrap().data.as_slice()
        );
        assert_eq!(
            "value_99".as_bytes(),
            collection.get("id_99").unwrap().data.as_slice()
        );
    }

    #[test]
    fn lookups_none() {
        let collection = super::create_test_collection(100);
        assert_eq!(100, collection.len());

        assert_eq!(None, collection.get("unknown"));
        assert_eq!(None, collection.get("id_100"));
    }

    #[test]
    fn insert() {
        let mut collection = KvCollection::new();

        collection.insert(super::KvElement::from_string(
            String::from("id_1"),
            String::from("value_1"),
        ));

        let element = collection.map.get("id_1").unwrap();
        assert_eq!("value_1".as_bytes(), element.data.as_slice());
        assert_eq!("text/plain", &element.mime);
        assert_eq!(0, element.last_modified);
        assert_eq!(0, element.expiration);
    }

    #[test]
    fn update() {
        let mut collection = KvCollection::new();

        collection.insert(super::KvElement::from_string(
            String::from("id_1"),
            String::from("value_1"),
        ));
        collection.insert(super::KvElement::from_string(
            String::from("id_1"),
            String::from("value_again"),
        ));
        let now = utils::now_as_millis();

        let element = collection.map.get("id_1").unwrap();
        assert_eq!("value_again".as_bytes(), element.data.as_slice());
        assert_eq!(now, element.last_modified);
    }

    #[test]
    fn remove() {
        let mut collection = super::create_test_collection(10);
        assert_eq!(10, collection.len());
        assert_eq!(
            "value_5".as_bytes(),
            collection.get("id_5").unwrap().data.as_slice()
        );

        let removed = collection.remove("id_5");
        assert_eq!(9, collection.len());
        assert_eq!("value_5".as_bytes(), removed.unwrap().data.as_slice());
        assert_eq!(None, collection.get("id_5"));
    }

    #[test]
    fn clear() {
        let mut collection = super::create_test_collection(10);
        assert_eq!(10, collection.len());
        assert_eq!(
            "value_5".as_bytes(),
            collection.get("id_5").unwrap().data.as_slice()
        );

        collection.clear();
        assert_eq!(0, collection.len());
        assert_eq!(None, collection.get("id_5"));
    }

    #[test]
    fn clear_expired() {
        let mut elt1 = super::KvElement::from_string(String::from("id_1"), String::from("value_1"));
        let mut elt2 = super::KvElement::from_string(String::from("id_2"), String::from("value_2"));
        let elt3 = super::KvElement::from_string(String::from("id_3"), String::from("value_3"));
        elt1.set_ttl(1);
        elt2.set_ttl(10000);

        let mut collection = KvCollection::new();
        collection.insert(elt1);
        collection.insert(elt2);
        collection.insert(elt3);
        println!("Initial -> {:?}", collection);
        assert_eq!(3, collection.len());

        utils::sleep(100);
        collection.clear_expired();
        println!("Cleaned -> {:?}", collection);
        assert_eq!(2, collection.len());
        assert_eq!(None, collection.get("id_1"));
        assert_ne!(None, collection.get("id_2"));
        assert_ne!(None, collection.get("id_3"));
    }

    #[test]
    fn iterator() {
        let collection = super::create_test_collection(10);
        assert_eq!(10, collection.iter().len());

        let mut it = collection.iter();
        let (k, v) = it.next().unwrap();
        assert_eq!("id_0", *k);
        assert_eq!("value_0".as_bytes(), v.data.as_slice());

        it.next();
        it.next();
        let (k, v) = it.next().unwrap();
        assert_eq!("id_3", *k);
        assert_eq!("value_3".as_bytes(), v.data.as_slice());
    }
}

#[cfg(test)]
mod tests_ttl_container {

    #[test]
    fn split_off_elapsed() {
        use super::TtlContainer;
        use crate::storage::utils;

        let now = utils::now_as_millis();
        let one_sec_before = now - 1000;
        let one_sec_later = now + 1000;

        let mut ids_by_timestamp = TtlContainer::new();
        ids_by_timestamp.insert(one_sec_before, String::from("abc"));
        ids_by_timestamp.insert(one_sec_before, String::from("def"));
        ids_by_timestamp.insert(now, String::from("ghi"));
        ids_by_timestamp.insert(one_sec_later, String::from("jkl"));
        println!("Initial -> {:?}", ids_by_timestamp);
        assert_eq!(3, ids_by_timestamp.ids_by_timestamp.len());

        let elapsed = ids_by_timestamp.split_off_elapsed(&now);
        println!("Split off -> {:?}", ids_by_timestamp);
        assert_eq!(1, elapsed.len());
        assert!(elapsed.contains_key(&one_sec_before));

        assert_eq!(2, ids_by_timestamp.ids_by_timestamp.len());
        assert!(ids_by_timestamp.ids_by_timestamp.contains_key(&now));
        assert!(ids_by_timestamp
            .ids_by_timestamp
            .contains_key(&one_sec_later));
    }
}
