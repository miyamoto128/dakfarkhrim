pub fn benchmark_collection() -> String {
    extern crate benchmarking;

    use super::{front, mem};

    const MAX_ELEMENTS: usize = 10_000;

    fn generate_kv_pair(i: usize) -> (String, String) {
        (format!("id_{}", i), format!("value_{}", i))
    }

    benchmarking::warm_up();

    let bench_result = benchmarking::measure_function_n(6, |measurers| {
        let mut collection = mem::KvCollection::new();

        // Measure adding
        for i in 0..MAX_ELEMENTS {
            let (key, value) = generate_kv_pair(i);
            measurers[0].measure(|| {
                collection.insert(front::KvElement::from_string(key, value));
            });
        }

        // Measure updating
        for i in 0..MAX_ELEMENTS {
            let (key, value) = generate_kv_pair(i);
            let value = value.to_uppercase(); // Value to uppercase, to ensure different value
            measurers[1].measure(|| {
                collection.insert(front::KvElement::from_string(key, value));
            });
        }

        // Measure reading
        for i in 0..MAX_ELEMENTS {
            let (key, _value) = generate_kv_pair(i);
            measurers[2].measure(|| {
                collection.get(&key);
            });
        }

        // Measure reading, unknown keys
        for i in 0..MAX_ELEMENTS {
            let (_value, key) = generate_kv_pair(i); // Invert keys and values to produce unknown keys
            measurers[3].measure(|| {
                collection.get(&key);
            });
        }

        // Measure swapping
        let mut collection2 = mem::create_test_collection(MAX_ELEMENTS);
        measurers[4].measure(|| {
            std::mem::swap(&mut collection, &mut collection2);
        });

        // Measure cleaning
        measurers[5].measure(|| {
            collection.clear();
        });
    })
    .unwrap();

    let mut report = String::new();
    report += format!("{} elements\n\n", MAX_ELEMENTS).as_str();
    report += format!("* adding       takes {:?}\n", bench_result[0].elapsed()).as_str();
    report += format!("* updating     takes {:?}\n", bench_result[1].elapsed()).as_str();
    report += format!("* finding      takes {:?}\n", bench_result[2].elapsed()).as_str();
    report += format!("* rejecting    takes {:?}\n", bench_result[3].elapsed()).as_str();
    report += format!("* swapping all takes {:?}\n", bench_result[4].elapsed()).as_str();
    report += format!("* clearing all takes {:?}\n", bench_result[5].elapsed()).as_str();

    report
}
