extern crate systemstat;
use systemstat::{System, Platform};


use std::time::SystemTime;

// Now timestamp in milliseconds
pub fn now_as_millis() -> u64 {
    let timestamp_ms = match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
        Ok(duration) => duration.as_millis(),
        Err(_) => 0,
    };
    timestamp_ms as u64
}

pub fn memory_available() -> u64 {
    let sys = System::new();
    match sys.memory() {
        Ok(mem) => mem.free.as_u64(),
        Err(_) => 0,
    }
}

#[cfg(test)]
pub fn sleep(duration_millis: u64) {
    use std::{thread, time};
    thread::sleep(time::Duration::from_millis(duration_millis));
}


