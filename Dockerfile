FROM rust:1.42-stretch

RUN apt-get update && apt-get install -y \
    openssl \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/dakfarkhrim

COPY src src
COPY Cargo.toml *.pem ./

RUN cargo install --path . \
    && mkdir /var/lib/dakfarkhrim

EXPOSE 8080/tcp
EXPOSE 8443/tcp

CMD ["dakfarkhrim"]
